import Sheet = GoogleAppsScript.Spreadsheet.Sheet;
import Spreadsheet = GoogleAppsScript.Spreadsheet.Spreadsheet;

// Code at https://gitlab.com/dbgit/open/google-sheets-macros/crypto-fund-update

class IndexUpdates {
  private currentSheet: Spreadsheet;
  private allSheets: Sheet[];

  constructor() {
    this.currentSheet = SpreadsheetApp.getActiveSpreadsheet();
    this.allSheets = this.currentSheet.getSheets();
  }

  public updatePositions() {
    const mutationsSheet = this.allSheets.find(sheet => sheet.getName() === 'Mutationen');
    if (!mutationsSheet) {
      Browser.msgBox('Es gibt keinen sheet mit dem namen "Mutationen"');
      return;
    }

    const positionsSheet = this.allSheets.find(sheet => sheet.getName() === 'Positionen');
    if (!positionsSheet) {
      Browser.msgBox('Es gibt keinen sheet mit dem namen "Positionen"');
      return;
    }

    const columns = mutationsSheet.getRange(2, 1, 100, 3).getValues();
    const positions = columns.reduce<Record<string, Position>>((res, [date, coin, mutationValue]) => {
      if (!coin) {
        return res;
      }

      if (!res[coin]) {
        res[coin] = {
          amount: 0,
          chartLink: `https://coinmarketcap.com/currencies/${coin}/`,
        }
      }

      const record = res[coin];
      record.amount += Number.parseFloat(mutationValue);

      return res;
    }, {});

    positionsSheet.getRange(2, 1, 100, 3).clearContent();

    const entries = Object.entries(positions);
    const positionRows = positionsSheet.getRange(2, 1, entries.length, 3);
    const values: [string, string, string][] = entries.map(([coin, {amount, chartLink}]) => ([coin, amount.toString(), chartLink]));

    positionRows.setValues(values)

    return;
  }
}

const updatePositions = () => new IndexUpdates().updatePositions();

interface Position {
  amount: number;
  chartLink: string;
}
